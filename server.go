// package main

// import (
// 	"net/http"

// 	"github.com/gin-gonic/gin"

// 	"gitlab.com/saiprasad_07/medium-go/entity"
// )

// func main() {
// 	server := gin.Default()

// 	server.GET("/api", func(ctx *gin.Context) {
// 		ctx.JSON(200, gin.H{
// 			"message": "OK",
// 		})
// 	})

// 	server.GET("/api/users", func(ctx *gin.Context) {
// 		ctx.JSON(http.StatusOK, entity.Users)
// 	})

// 	server.Run(":5000")
// }

package main
import (
	"gitlab.com/saiprasad_07/medium-go/routes"
	_ "github.com/go-sql-driver/mysql"
)
func main() {
	routes.CreateUrlMappings()
	// Listen and server on 0.0.0.0:8080
	routes.Router.Run(":5000")
}