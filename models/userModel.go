package models

type User struct {
	Id   string   `json:"id"`
	FullName   string   `json:"fullName"`
	HandleName string   `json:"handleName"`
	Email      string   `json:"email"`
	Password   string   `json:"password"`
	Location   string   `json:"location"`
	City       string   `json:"city"`
	Country    string   `json:"country"`
	// Interest   []string `json:"interest"`
}